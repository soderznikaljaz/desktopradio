﻿namespace OnlineRadio
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.trackBar1 = new System.Windows.Forms.TrackBar();
            this.pbRockRadio = new System.Windows.Forms.PictureBox();
            this.pbRadioCity = new System.Windows.Forms.PictureBox();
            this.btnPlayPause = new System.Windows.Forms.Button();
            this.pbRockMb = new System.Windows.Forms.PictureBox();
            this.lblStatus = new System.Windows.Forms.Label();
            this.timerStatus = new System.Windows.Forms.Timer(this.components);
            this.pbRadio1 = new System.Windows.Forms.PictureBox();
            this.pbAltr = new System.Windows.Forms.PictureBox();
            this.timerWeather = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbRockRadio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbRadioCity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbRockMb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbRadio1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbAltr)).BeginInit();
            this.SuspendLayout();
            // 
            // trackBar1
            // 
            this.trackBar1.LargeChange = 25;
            this.trackBar1.Location = new System.Drawing.Point(216, 68);
            this.trackBar1.Maximum = 100;
            this.trackBar1.Name = "trackBar1";
            this.trackBar1.Size = new System.Drawing.Size(150, 45);
            this.trackBar1.SmallChange = 10;
            this.trackBar1.TabIndex = 2;
            this.trackBar1.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trackBar1.Value = 75;
            this.trackBar1.ValueChanged += new System.EventHandler(this.TrackBar1_ValueChanged);
            // 
            // pbRockRadio
            // 
            this.pbRockRadio.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbRockRadio.ErrorImage = global::OnlineRadio.Properties.Resources.rockRadio;
            this.pbRockRadio.Image = ((System.Drawing.Image)(resources.GetObject("pbRockRadio.Image")));
            this.pbRockRadio.InitialImage = global::OnlineRadio.Properties.Resources.rockRadioON;
            this.pbRockRadio.Location = new System.Drawing.Point(216, 200);
            this.pbRockRadio.Name = "pbRockRadio";
            this.pbRockRadio.Size = new System.Drawing.Size(150, 150);
            this.pbRockRadio.TabIndex = 1;
            this.pbRockRadio.TabStop = false;
            this.pbRockRadio.Click += new System.EventHandler(this.selectStation);
            this.pbRockRadio.MouseEnter += new System.EventHandler(this.station_MouseEnter);
            this.pbRockRadio.MouseLeave += new System.EventHandler(this.station_MouseLeave);
            // 
            // pbRadioCity
            // 
            this.pbRadioCity.BackColor = System.Drawing.SystemColors.Control;
            this.pbRadioCity.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbRadioCity.ErrorImage = global::OnlineRadio.Properties.Resources.radioCity;
            this.pbRadioCity.Image = ((System.Drawing.Image)(resources.GetObject("pbRadioCity.Image")));
            this.pbRadioCity.InitialImage = global::OnlineRadio.Properties.Resources.radioCityON;
            this.pbRadioCity.Location = new System.Drawing.Point(25, 200);
            this.pbRadioCity.Name = "pbRadioCity";
            this.pbRadioCity.Size = new System.Drawing.Size(150, 150);
            this.pbRadioCity.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbRadioCity.TabIndex = 0;
            this.pbRadioCity.TabStop = false;
            this.pbRadioCity.Click += new System.EventHandler(this.selectStation);
            this.pbRadioCity.MouseEnter += new System.EventHandler(this.station_MouseEnter);
            this.pbRadioCity.MouseLeave += new System.EventHandler(this.station_MouseLeave);
            // 
            // btnPlayPause
            // 
            this.btnPlayPause.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPlayPause.BackgroundImage")));
            this.btnPlayPause.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPlayPause.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPlayPause.FlatAppearance.BorderSize = 0;
            this.btnPlayPause.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnPlayPause.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnPlayPause.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPlayPause.Location = new System.Drawing.Point(70, 49);
            this.btnPlayPause.Name = "btnPlayPause";
            this.btnPlayPause.Size = new System.Drawing.Size(64, 64);
            this.btnPlayPause.TabIndex = 3;
            this.btnPlayPause.UseVisualStyleBackColor = true;
            this.btnPlayPause.Click += new System.EventHandler(this.BtnPlayPause_Click);
            // 
            // pbRockMb
            // 
            this.pbRockMb.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbRockMb.ErrorImage = ((System.Drawing.Image)(resources.GetObject("pbRockMb.ErrorImage")));
            this.pbRockMb.Image = ((System.Drawing.Image)(resources.GetObject("pbRockMb.Image")));
            this.pbRockMb.InitialImage = ((System.Drawing.Image)(resources.GetObject("pbRockMb.InitialImage")));
            this.pbRockMb.Location = new System.Drawing.Point(409, 200);
            this.pbRockMb.Name = "pbRockMb";
            this.pbRockMb.Size = new System.Drawing.Size(150, 150);
            this.pbRockMb.TabIndex = 4;
            this.pbRockMb.TabStop = false;
            this.pbRockMb.Click += new System.EventHandler(this.selectStation);
            this.pbRockMb.MouseEnter += new System.EventHandler(this.station_MouseEnter);
            this.pbRockMb.MouseLeave += new System.EventHandler(this.station_MouseLeave);
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblStatus.Location = new System.Drawing.Point(74, 139);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(0, 24);
            this.lblStatus.TabIndex = 5;
            // 
            // pbRadio1
            // 
            this.pbRadio1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbRadio1.ErrorImage = ((System.Drawing.Image)(resources.GetObject("pbRadio1.ErrorImage")));
            this.pbRadio1.Image = ((System.Drawing.Image)(resources.GetObject("pbRadio1.Image")));
            this.pbRadio1.InitialImage = ((System.Drawing.Image)(resources.GetObject("pbRadio1.InitialImage")));
            this.pbRadio1.Location = new System.Drawing.Point(25, 388);
            this.pbRadio1.Name = "pbRadio1";
            this.pbRadio1.Size = new System.Drawing.Size(150, 150);
            this.pbRadio1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbRadio1.TabIndex = 6;
            this.pbRadio1.TabStop = false;
            this.pbRadio1.Click += new System.EventHandler(this.selectStation);
            this.pbRadio1.MouseEnter += new System.EventHandler(this.station_MouseEnter);
            this.pbRadio1.MouseLeave += new System.EventHandler(this.station_MouseLeave);
            // 
            // pbAltr
            // 
            this.pbAltr.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbAltr.ErrorImage = ((System.Drawing.Image)(resources.GetObject("pbAltr.ErrorImage")));
            this.pbAltr.Image = ((System.Drawing.Image)(resources.GetObject("pbAltr.Image")));
            this.pbAltr.InitialImage = ((System.Drawing.Image)(resources.GetObject("pbAltr.InitialImage")));
            this.pbAltr.Location = new System.Drawing.Point(216, 388);
            this.pbAltr.Name = "pbAltr";
            this.pbAltr.Size = new System.Drawing.Size(150, 150);
            this.pbAltr.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbAltr.TabIndex = 7;
            this.pbAltr.TabStop = false;
            this.pbAltr.Click += new System.EventHandler(this.selectStation);
            this.pbAltr.MouseEnter += new System.EventHandler(this.station_MouseEnter);
            this.pbAltr.MouseLeave += new System.EventHandler(this.station_MouseLeave);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(587, 568);
            this.Controls.Add(this.pbAltr);
            this.Controls.Add(this.pbRadio1);
            this.Controls.Add(this.lblStatus);
            this.Controls.Add(this.pbRockMb);
            this.Controls.Add(this.btnPlayPause);
            this.Controls.Add(this.trackBar1);
            this.Controls.Add(this.pbRockRadio);
            this.Controls.Add(this.pbRadioCity);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Digitalni radio";
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbRockRadio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbRadioCity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbRockMb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbRadio1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbAltr)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pbRadioCity;
        private System.Windows.Forms.PictureBox pbRockRadio;
        private System.Windows.Forms.TrackBar trackBar1;
        private System.Windows.Forms.Button btnPlayPause;
        private System.Windows.Forms.PictureBox pbRockMb;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.Timer timerStatus;
        private System.Windows.Forms.PictureBox pbRadio1;
        private System.Windows.Forms.PictureBox pbAltr;
        private System.Windows.Forms.Timer timerWeather;
    }
}

