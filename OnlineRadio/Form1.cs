﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OnlineRadio
{
    public partial class Form1 : Form
    {
        public static WMPLib.WindowsMediaPlayer player = new WMPLib.WindowsMediaPlayer();
        public string selectedStation = string.Empty;
        public bool isPlaying = false;
        public Form1()
        {
            InitializeComponent();
            pbRadioCity.Image = pbRadioCity.ErrorImage;
            selectedStation = pbRadioCity.Name;
            player.settings.autoStart = false;
            player.URL = Properties.Resources.radioCityURL;

            //timer for status label
            timerStatus.Tick += timerStatus_Tick;
            timerStatus.Interval = 500;
            timerStatus.Enabled = true;

            //timer for weather info
            timerWeather.Tick += TimerWeather_Tick;
            timerWeather.Interval = 60000;
            timerStatus.Enabled = true;
        }

        private void TimerWeather_Tick(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        private void timerStatus_Tick(object sender, EventArgs e)
        {
            lblStatus.Text = player.status;
        }

        private void station_MouseEnter(object sender, EventArgs e)
        {
            (sender as PictureBox).Image = (sender as PictureBox).ErrorImage;
        }

        private void station_MouseLeave(object sender, EventArgs e)
        {
            if (selectedStation != (sender as PictureBox).Name)
            {
                (sender as PictureBox).Image = (sender as PictureBox).InitialImage;
            }          
        }

        private void TrackBar1_ValueChanged(object sender, EventArgs e)
        {
            player.settings.volume = (sender as TrackBar).Value;
        }

        private void BtnPlayPause_Click(object sender, EventArgs e)
        {
            if (!isPlaying)
            {
                btnPlayPause.BackgroundImage = Properties.Resources.pause_button;
                player.controls.play();
                isPlaying = true;
            }
            else
            {
                btnPlayPause.BackgroundImage = Properties.Resources.play_button;
                player.controls.stop();
                isPlaying = false;
            }
        }

        private void selectStation(object sender, EventArgs e)
        {
            PictureBox pb = sender as PictureBox;
            if (pb.Name != selectedStation)
            {
                switch (pb.Name)
                {
                    case "pbRadioCity":
                        player.URL = Properties.Resources.radioCityURL;
                        break;
                    case "pbRockRadio":
                        player.URL = Properties.Resources.rockRadioURL;
                        break;
                    case "pbRockMb":
                        player.URL = Properties.Resources.rockMariborURL;
                        break;
                    case "pbRadio1":
                        player.URL = Properties.Resources.radio1URL;
                        break;
                    case "pbAltr":
                        player.URL = Properties.Resources.altrURL;
                        break;
                    default:
                        break;
                }
                if (isPlaying)
                {
                    player.controls.play();
                }
                PictureBox prevPb = (PictureBox)Controls.Find(selectedStation, false).FirstOrDefault();
                prevPb.Image = prevPb.InitialImage;
                selectedStation = pb.Name;
                pb.Image = pb.ErrorImage;
            }

        }
    }
}
